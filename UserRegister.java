package bcas.assi.bookstore;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class UserRegister extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JPasswordField passwordField_1;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserRegister frame = new UserRegister();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserRegister() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLUE);
		contentPane.setBackground(Color.PINK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("User Register");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(129, 22, 193, 26);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_2 = new JLabel("User Name");
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setBounds(33, 96, 79, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Status");
		lblNewLabel_3.setForeground(Color.BLUE);
		lblNewLabel_3.setBounds(34, 194, 59, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("address");
		lblNewLabel_4.setForeground(Color.BLUE);
		lblNewLabel_4.setBounds(34, 169, 59, 14);
		contentPane.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("Password");
		lblNewLabel_5.setForeground(Color.BLUE);
		lblNewLabel_5.setBounds(33, 146, 59, 14);
		contentPane.add(lblNewLabel_5);

		textField_1 = new JTextField();
		textField_1.setBounds(103, 90, 138, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(103, 118, 138, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(103, 166, 138, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		JButton btnNewButton = new JButton("Submit");
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String name = textField_1.getText();
				String password = String.valueOf(passwordField_1.getPassword());
				String email = textField_2.getText();
				String address = textField_3.getText();
				String status = textField_4.getText();

				int i = UserDbAdd.save(name, password, email, address, status);
				if (i > 0) {
					JOptionPane.showMessageDialog(UserRegister.this, "User added successfully!");
					Login.main(new String[] {});

				} else {
					JOptionPane.showMessageDialog(UserRegister.this, "Sorry, unable to save!");
				}

			}

		});
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setBounds(298, 209, 89, 23);
		contentPane.add(btnNewButton);

		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(102, 143, 138, 20);
		contentPane.add(passwordField_1);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(103, 189, 138, 20);
		contentPane.add(textField_4);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setForeground(Color.BLUE);
		lblEmail.setBounds(34, 121, 59, 14);
		contentPane.add(lblEmail);
	}
}

package bcas.assi.bookstore;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ConfirmOrder extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnCard;
	private JButton btnCash;
	private JTextField textField;
	static double total;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConfirmOrder frame = new ConfirmOrder();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConfirmOrder() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 473, 384);
		contentPane = new JPanel();
		contentPane.setBackground(Color.PINK);
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		String data[][] = null;
		String column[] = null;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps1 = con.prepareStatement("select BookPrice from addbook where bookid=?");
			ps1.setInt(1, OrderBook.bookId);
			ResultSet rs1 = ps1.executeQuery();
			rs1.last();
			total = Double.parseDouble(rs1.getString(1)) * OrderBook.qty;

			rs1.beforeFirst();
			// textField.setText(String.valueOf(total));

			PreparedStatement ps = con.prepareStatement(
					"select BookName,Auther,Category,BookPrice from addbook where bookid=?",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ps.setInt(1, OrderBook.bookId);
			ResultSet rs = ps.executeQuery();

			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();
			column = new String[cols];
			for (int i = 1; i <= cols; i++) {
				column[i - 1] = rsmd.getColumnName(i);
			}

			rs.last();
			int rows = rs.getRow();
			rs.beforeFirst();

			data = new String[rows][cols];
			int count = 0;
			while (rs.next()) {
				for (int i = 1; i <= cols; i++) {
					data[count][i - 1] = rs.getString(i);
				}
				count++;
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		contentPane.setLayout(null);

		table = new JTable(data, column);
		JScrollPane sp = new JScrollPane(table);
		sp.setBounds(26, 11, 406, 205);

		contentPane.add(sp);

		btnCard = new JButton("Card");
		btnCard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardDetail.main(new String[] {});
			}
		});
		btnCard.setForeground(Color.BLUE);
		btnCard.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnCard.setBounds(316, 286, 89, 28);
		contentPane.add(btnCard);

		btnCash = new JButton("Cash");
		btnCash.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnCash.setForeground(Color.BLUE);
		btnCash.setBounds(189, 288, 79, 28);
		contentPane.add(btnCash);

		textField = new JTextField();
		textField.setBounds(291, 251, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		JLabel lblTotalRs = new JLabel("Total RS");
		lblTotalRs.setBounds(244, 254, 46, 14);
		contentPane.add(lblTotalRs);
		textField.setText(String.valueOf(total));
	}
}

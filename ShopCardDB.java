package bcas.assi.bookstore;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ShopCardDB {

	public static boolean checkBook(int bookId) {
		boolean status = false;
		try {
			Connection con = DB.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from addbook where BookId=?");
			ps.setInt(1, bookId);
			ResultSet rs = ps.executeQuery();
			status = rs.next();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int save(int bookId) {
		int status = 0;
		String bName, auther, category;
		double price;
		try {
			Connection con = DB.getConnection();

			status = updatebook(bookId);// updating quantity and issue
			PreparedStatement ps1 = con
					.prepareStatement("select bookName,Auther,Category,BookPrice from addBook where bookId=?");
			ps1.setInt(1, bookId);
			ResultSet rs = ps1.executeQuery();
			rs.last();
			bName = rs.getString(1);
			auther = rs.getString(2);
			category = rs.getString(3);
			price = Double.parseDouble(rs.getString(4));

			rs.beforeFirst();

			if (status > 0) {
				PreparedStatement ps = con.prepareStatement(
						"insert into ShoppingCart(CustomerName,BookName,Auther,Category,Quantity,TotalPrice) values(?,?,?,?,?,?)");
				ps.setString(1, Login.user);
				ps.setString(2, bName);
				ps.setString(3, auther);
				ps.setString(4, category);
				ps.setInt(5, OrderBook.qty);
				ps.setDouble(6, price * OrderBook.qty);
				status = ps.executeUpdate();
			}

			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int updatebook(int bookId) {
		int status = 0;
		int quantity = 0;// issued = 0;
		try {
			Connection con = DB.getConnection();

			PreparedStatement ps = con.prepareStatement("select quantity from addbook where bookId=?");
			ps.setInt(1, bookId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				quantity = rs.getInt("quantity");
				// issued = rs.getInt("issued");
			}

			if (quantity > 0) {
				PreparedStatement ps2 = con.prepareStatement("update addbook set quantity=? where bookId=?");
				ps2.setInt(1, quantity - 1);
				// ps2.setInt(2, issued + 1);
				ps2.setInt(2, bookId);

				status = ps2.executeUpdate();
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}
}
